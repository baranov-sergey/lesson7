Дополнение к заданию 2 домашней работы номер №7
###

Реализуйте стратегию оплаты, используя интерфейс IPaymentStrategy. Оплата может быть, к примеру, через ApplePay или картой — на ваше усмотрение. Соответственно, в IPaymentStrategy есть метод ProcessPayment(decimal amount). Создайте несколько реализаций IPaymentStrategy.

Также создайте интерфейс ILogger с методом Log и сделайте одну реализацию логирования в консоль (просто вывести в консоль с помощью Console.WriteLine) и вторую реализацию для логирования в файл (сообщение в консоли, что логирование производится в файл).

Создайте класс PaymentProcessor, который будет принимать в конструктор IPaymentStrategy и ILogger. Добавьте метод ProcessPayment в классе PaymentProcessor, который будет логировать и выполнять метод ProcessPayment интерфейса IPaymentStrategy.

Создайте экземпляр PaymentProcessor, а также несколько экземпляров различных видов оплат и разных логгеров.

Код должен выглядеть примерно так:

IPaymentStrategy creditCardPayment = new CreditCardPayment();
ILogger consoleLogger = new ConsoleLogger();
PaymentProcessor processor1 = new PaymentProcessor(creditCardPayment, consoleLogger);
processor1.ProcessPayment(amount);

// Using ApplePayPayment and FileLogger
IPaymentStrategy applePayPayment = new ApplePayPayment();
ILogger fileLogger = new FileLogger();
PaymentProcessor processor2 = new PaymentProcessor(applePayPayment, fileLogger);
processor2.ProcessPayment(amount);

##