﻿using Lesson7.Cars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7
{
    internal class ExampleCars
    {
        /*
            ### 4. Машинки
            Полную структуру классов и их взаимосвязь продумать самостоятельно.
            Исходные данные база (массив) из n машин задать непосредственно в коде
            Создать базовый класс Auto с методами позволяющим вывести на экран информацию о транспортном средстве, а также определить грузоподъемность транспортного средства.
            Создать производные классы: 
            Легковая_машина (марка, номер, скорость, грузоподъемность),
            Мотоцикл (марка, номер, скорость, грузоподъемность, наличие коляски, при этом если коляска отсутствует, то грузоподъемность равна 0), 
            Грузовик (марка, номер, скорость, грузоподъемность, наличие прицепа, при этом если есть прицеп, то грузоподъемность увеличивается в два раза) со своими методами вывода информации на экран, и определения грузоподъемности.
            Создать базу (массив) из n машин, вывести полную информацию из базы на экран, а также организовать поиск машин, удовлетворяющих требованиям грузоподъемности.
        */

        public static void Example()
        {
            Auto[] autos = {
            new PassengerCar("Audi", "К001АТ196", 300, 400),
            new Motorbike("Yamaha", "С002НО196", 400, 150, true),
            new Truck("Зил", "С002НО196", 200, 5000, true)
            };

            foreach (Auto auto in autos)
            {
                auto.TypeInfo();
            }
            Console.WriteLine();

            Console.WriteLine("Indicate the required load capacity:");

            uint liftingCapacity = default, cnt = default;
            while (true)
            {
                if (!uint.TryParse(Console.ReadLine(), out liftingCapacity))
                { Console.WriteLine("Incorrect value, please re-enter the operation"); continue; }
                Console.WriteLine();
                break;
            }

            foreach (Auto auto in autos)
            {
                if (auto.GetLoadCapacity() >= liftingCapacity)
                {
                    cnt++;
                    auto.TypeInfo();
                }
            }

            if (cnt == 0)
            {
                Console.WriteLine("No vehicles with permissible carrying capacity");
            }
        }
    }
}
