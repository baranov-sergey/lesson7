﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Discount
{
    internal class FixedDiscount : DiscountStrategy
    {
        public override string DiscountName { get; } = "fixed discount";
        private double fixDiscount = default;
        public FixedDiscount(double fixDiscount)
        {
            this.fixDiscount = fixDiscount;
        }
        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice - fixDiscount;
        }
    }
}
