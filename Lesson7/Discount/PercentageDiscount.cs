﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Discount
{
    internal class PercentageDiscount : DiscountStrategy
    {
        public override string DiscountName { get; } = "percentage discount";
        private double percentDiscount = default;
        public PercentageDiscount(double percentDiscount)
        {
            this.percentDiscount = percentDiscount;
        }
        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice - (percentDiscount * originalPrice / 100);
        }
    }
}
