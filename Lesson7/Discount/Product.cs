﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Discount
{
    internal class Product
    {
        public string name { get; private set; } = string.Empty;
        public double price { get; private set; } = default;           //USD
        public Product(string name, double price)
        {
            this.name = name;
            this.price = price;
        }
    }
}
