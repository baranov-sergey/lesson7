﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Discount
{
    internal abstract class DiscountStrategy
    {
        public virtual string DiscountName { get; }
        public virtual double ApplyDiscount(double originalPrice)
        {
            return originalPrice;
        }
    }
}
