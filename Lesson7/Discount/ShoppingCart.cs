﻿using Lesson7.Payment;

namespace Lesson7.Discount
{
    internal class ShoppingCart
    {
        public DiscountStrategy discountStrategy { get; private set; }
        private Product[] products = default;
        PaymentProcessor paymentProcessor = default;

        public ShoppingCart(Product[] products, PaymentProcessor paymentProcessor, DiscountStrategy discountStrategy = default)
        {
            this.products = products;
            this.discountStrategy = discountStrategy;
            this.paymentProcessor = paymentProcessor;
        }

        public void CalculateTotal()
        {
            foreach (var product in products)
            {
                Console.WriteLine($"for product: {product.name}, " +
                $"total amount was: {discountStrategy.ApplyDiscount(product.price)}, " +
                $"discount name: {discountStrategy.DiscountName}");
                paymentProcessor.ProcessPayment((decimal)product.price);
            }
        }
    }
}
