﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.AnimalNursery
{
    internal class PetShop
    {
        public Animal[] animals { get; }
        public PetShop(Animal[] animals)
        {
            this.animals = animals;
        }
        public void MakeAllSounds()
        {
            foreach (var anim in animals)
            {
                anim.MakeSound();
            }
        }
    }
}
