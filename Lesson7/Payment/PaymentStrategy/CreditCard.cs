﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Payment.PaymentStrategy
{
    internal class CreditCard : IPaymentStrategy
    {
        public void ProcessPayment(decimal amount)
        {
            Console.WriteLine($"Credit card payment amount: {amount}");
        }
    }
}
