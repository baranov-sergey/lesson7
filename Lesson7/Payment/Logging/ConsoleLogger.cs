﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Payment.Logging
{
    internal class ConsoleLogger : ILogger
    {
        public void Logging()
        {
            Console.WriteLine($"{DateTime.Now} logging the operation");
        }
    }
}
