﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Payment.Logging
{
    internal class LoggerToFile : ILogger
    {
        public void Logging()
        {
            Console.WriteLine("The log is written to a file");
        }
    }
}
