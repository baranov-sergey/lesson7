﻿using Lesson7.Payment.Logging;
using Lesson7.Payment.PaymentStrategy;

namespace Lesson7.Payment
{
    internal class PaymentProcessor
    {
        IPaymentStrategy pay = default;
        ILogger log = default;

        public PaymentProcessor(IPaymentStrategy pay, ILogger log)
        {
            this.pay = pay;
            this.log = log;
        }

        public void ProcessPayment(decimal amount)
        {
            pay.ProcessPayment(amount);
            log.Logging();
        }
    }
}