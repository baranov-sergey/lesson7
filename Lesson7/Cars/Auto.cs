﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Cars
{
    public class Auto
    {
        public string brand { get; }
        public string number { get; }
        public uint speed { get; }
        public uint liftingCapacity { get; protected set; }

        public Auto(string brand, string number, uint speed, uint liftingCapacity)
        {
            this.brand = brand;
            this.number = number;
            this.speed = speed;
            this.liftingCapacity = liftingCapacity;
        }

        public virtual uint GetLoadCapacity()
        {
            return liftingCapacity;
        }

        public virtual void TypeInfo()
        {
            Console.WriteLine($"Brand: {brand}, number: {number}, speed: {speed}, liftingCapacity: {liftingCapacity}");
        }
    }
}
