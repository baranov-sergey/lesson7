﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Cars
{
    internal class Motorbike : Auto
    {
        public bool sidecar { get; }

        public Motorbike(string brand, string number, uint speed, uint liftingCapacity, bool sidecar) : base(brand, number, speed, liftingCapacity)
        {
            this.sidecar = sidecar;
        }

        public override void TypeInfo()
        {
            Console.WriteLine($"Brand: {brand}, number: {number}, speed: {speed}, liftingCapacity: {GetLoadCapacity()}, sidecar: {(sidecar == true ? '+' : '-')}");
        }

        public override uint GetLoadCapacity()
        {
            if (!sidecar)
            {
                this.liftingCapacity = 0;
            }

            return liftingCapacity;
        }
    }
}
