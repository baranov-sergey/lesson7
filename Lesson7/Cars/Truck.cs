﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Cars
{
    internal class Truck : Auto
    {
        public bool trailer { get; }

        public Truck(string brand, string number, uint speed, uint liftingCapacity, bool trailer) : base (brand, number, speed, liftingCapacity)
        {
            this.trailer = trailer;
        }

        public override void TypeInfo()
        {
            Console.WriteLine($"Brand: {brand}, number: {number}, speed: {speed}, liftingCapacity: {GetLoadCapacity()}, trailer: {(trailer == true ? '+' : '-')}");
        }

        public override uint GetLoadCapacity()
        {
            if (trailer)
            {
                base.liftingCapacity *= 2;
            }

            return liftingCapacity;
        }
    }
}
