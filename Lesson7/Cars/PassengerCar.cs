﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Cars
{
    internal class PassengerCar : Auto
    {
        public PassengerCar(string brand, string number, uint speed, uint liftingCapacity) : base(brand, number, speed, liftingCapacity)
        {

        }

        public override void TypeInfo()
        {
            Console.WriteLine($"Brand: {brand}, number: {number}, speed: {speed}, liftingCapacity: {liftingCapacity}");
        }
    }
}
