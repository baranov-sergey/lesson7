﻿using Lesson7.AnimalNursery;
using Lesson7.CarPark;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7
{
    internal class ExampleCarPark
    {
        /*
            ✨3. Автопарк✨
            - Создать класс, cо следующими свойствами: 
            сlass Bus: Пункт назначения, Номер, Время отправления, Число мест
            Определить get, set методы для этих свойств.
            Создать класс с main методом, в котором будет объявлен объект класса Bus. 
            Вывести все данные (значения полей) объекта в консоль. 
            
            - Определить иерархию классов в предметной области: Парк общественного траспорта. 
            Определить иерархию различных видов общественного транспорта (например, Trolleybus, Tramcar и т.д.
            на подобие класса Bus выше.) 
            Определить в суперклассе (например, сlass Transport) метод, возвращающий тип транспорта (Electric, Rail, и т.п.). 
            Переопределить этот метод в классах наследниках.
            
            - В классе с main методом создать массив объектов из различных видов транспорта.
            Провести сортировку транспорта по количеству мест и вывести данные объектов в консоль.
            Запросить у пользователя время отправления и/или пункт назначения. Найти в массиве объект,
            соответствующий заданным параметрам, и вывести его данные в консоль. 
            Запросить у пользователя время отправления. Вывести в консоль список транспорта, отправляющегося после заданного времени.
            
            -  Выбрать метод в супеклассе (e.g. Transport), который нельзя будет переопределить в классах наследниках
            и запретить это переопределение.
            Создать класс TransportService. Определить внутри класса TransportService метод printTransportType,
            который будет принимать объект типа Transport как параметр. 
            Внутри метода printTransportType необходимо вызвать другой метод, возвращающий тип транспорта
            и вывести эту информацию на консоль. 
            ** В main вызвать метод printTransportType несколько раз,
            *передавая ему как параметр объекты классов наследников (Bus, Trolleybus и т.д.)
        */

        public static void Example()
        {
            //Вывести все данные (значения полей) объекта в консоль. 
            Console.WriteLine("Outputting object data:");
            Bus bus = new Bus("Moscow", "A777-1", new DateTime(2023, 12, 1), 10);
            Console.WriteLine($"Destination: {bus.destination}, " +
            $"number: {bus.number}, " +
            $"departureTime: {bus.departureTime}, " +
            $"numberOfSeats: {bus.numberOfSeats}\n");

            //Провести сортировку транспорта по количеству мест и вывести данные объектов в консоль.
            AutomobileTransport[] automobileTransports = {
            new Bus("Moscow", "A777-1", new DateTime(2023, 12, 1, 8, 30, 0), 40),
            new Trolleybus("Broadway", "5b", new DateTime(2023, 10, 5, 12, 40, 10), 30),
            new Tramcar("Central Avenue", "20", new DateTime(2023, 8, 1, 20, 55, 40), 20)
            };

            SortingAutomobileTransportByNumberOfSeats(automobileTransports);

            Console.WriteLine("Output of transport sorting by number of seats:");
            foreach (var automobileTransport in automobileTransports)
            {
                Console.WriteLine($"Destination: {automobileTransport.destination}, " +
                $"number: {automobileTransport.number}, " +
                $"departureTime: {automobileTransport.departureTime}, " +
                $"numberOfSeats: {automobileTransport.numberOfSeats}");
            }
            Console.WriteLine();

            //Запросить у пользователя время отправления и/ или пункт назначения. Найти в массиве объект, соответствующий заданным параметрам, и вывести его данные в консоль.
            while (true)
            {
                Console.WriteLine("Enter your destination:");
                string destination = Console.ReadLine().ToLower();
                int cnt = default;

                for (int i = 0; i < automobileTransports.Length; i++)
                {
                    if (destination == automobileTransports[i].destination.ToLower())
                    {
                        cnt++;
                        if (cnt == 1) { Console.WriteLine("Suitable destinations:"); }
                        Console.WriteLine(
                        $"Destination: {automobileTransports[i].destination}, " +
                        $"number: {automobileTransports[i].number}, " +
                        $"departureTime: {automobileTransports[i].departureTime}, " +
                        $"numberOfSeats: {automobileTransports[i].numberOfSeats}");
                    }
                }
                Console.WriteLine();

                if (cnt == 0)
                {
                    Console.WriteLine("There is no such destination, please re-enter\n"); continue;
                }

                break;
            }

            //Запросить у пользователя время отправления.Вывести в консоль список транспорта, отправляющегося после заданного времени.
            DateTime dateUser = default;
            while (true)
            {
                Console.WriteLine("Enter the departure date and time:");
                if (!DateTime.TryParse(Console.ReadLine(), out dateUser))
                {
                    Console.WriteLine("Invalid value, re-enter\n");
                    continue;
                }
                else
                {
                    Console.WriteLine();
                    break;
                }
            }

            Console.WriteLine("Available departure times:");
            foreach (var automobileTransport in automobileTransports)
            {
                if (dateUser <= automobileTransport.departureTime)
                { Console.WriteLine(
                    $"Destination: {automobileTransport.destination}, " +
                    $"number: {automobileTransport.number}, " +
                    $"departureTime: {automobileTransport.departureTime}, " +
                    $"numberOfSeats: {automobileTransport.numberOfSeats}"); 
                }
            }
            Console.WriteLine();

            //В main вызвать метод printTransportType несколько раз,
            //*передавая ему как параметр объекты классов наследников(Bus, Trolleybus и т.д.)
            Transport[] transports = {
            new Bus("Moscow", "A777-1", new DateTime(2023, 12, 1, 8, 30, 0), 40),
            new Trolleybus("Broadway", "5b", new DateTime(2023, 10, 5, 12, 40, 10), 30),
            new Tramcar("Central Avenue", "20", new DateTime(2023, 8, 1, 20, 55, 40), 20),
            new Ship("NY", "D345FV", new DateTime(2023, 11, 18, 14, 20, 00), 1000),
            new Airplane("LA", "777G", new DateTime(2023, 5, 2, 12, 0, 0), 200)
            };
            TransportService transportService = new TransportService();
            transportService.printTransportType(transports[0]);
            transportService.printTransportType(transports[3]);
            transportService.printTransportType(transports[4]);
        }

        public static void SortingAutomobileTransportByNumberOfSeats(AutomobileTransport[] automobileTransports)
        {
            for (int i = automobileTransports.Length - 1; i > 0; i--)   // на какое место будем ставим наибольший элемент
            {
                for (int j = 0; j < i; j++)   // проходим по не отсортированной последовательности
                {
                    if (automobileTransports[j].numberOfSeats > automobileTransports[j + 1].numberOfSeats)  // если порядок элементов неправильный
                    {
                        // меняем местами пары
                        var temp = automobileTransports[j];
                        automobileTransports[j] = automobileTransports[j + 1];
                        automobileTransports[j + 1] = temp;
                    }
                }
            }
        }
    }
}
