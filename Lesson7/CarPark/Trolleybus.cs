﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.CarPark
{
    internal class Trolleybus : AutomobileTransport
    {
        public Trolleybus(string destination, string number, DateTime departureTime, int numberOfSeats) : base(destination, number, departureTime, numberOfSeats)
        {

        }
    }
}
