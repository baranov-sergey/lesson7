﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.CarPark
{
    internal class Transport
    {
        public string destination { get; } = string.Empty;
        public string number { get; } = string.Empty;
        public DateTime departureTime { get; } = default;
        public int numberOfSeats { get; } = default;

        public Transport(string destination, string number, DateTime departureTime, int numberOfSeats)
        {
            this.destination = destination;
            this.number = number;
            this.departureTime = departureTime;
            this.numberOfSeats = numberOfSeats;
        }

        public virtual string TypeTransport()
        {
            return "Transport";
        }

        public new void MethodTransport()
        {
            Console.WriteLine("Output MethodTransport");
        }
    }
}
