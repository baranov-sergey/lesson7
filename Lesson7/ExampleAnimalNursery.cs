﻿using Lesson7.AnimalNursery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7
{
    internal class ExampleAnimalNursery
    {
        /*
            ### 1. 
            Питомник животных
            Создайте абстрактный класс Animal с методом MakeSound().
            Создайте несколько классов животных (например, Dog, Cat), наследуя их от Animal и реализуя метод MakeSound().
            Создайте класс PetShop который принимает массив животных в конструктор
            Добавьте метод MakeAllSounds в PetShop для демонстрации их "голосов"
        */
       
        public static void Example()
        {
            Animal[] animals = { new Cat(), new Dog() };
            PetShop petShop = new PetShop(animals);
            petShop.MakeAllSounds();
            Console.WriteLine();
        }
    }
}
