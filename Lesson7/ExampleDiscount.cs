﻿using Lesson7.Discount;
using Lesson7.Payment;
using Lesson7.Payment.PaymentStrategy;
using Lesson7.Payment.Logging;

namespace Lesson7
{
    internal class ExampleDiscount
    {
        /*
            ###2
            Создать стратегию для предоставления дисконта: либо фиксированная сумма, либо процентная.
            Создать базовый класс DiscountStrategy с виртуальным методом ApplyDiscount, принимающим originalPrice.
            Базовая реализация виртуального метода подразумевает, что никакой дисконт применяться не будет, следовательно, просто возвращаем цену.      
            Создать два класса: FixedDiscount (вычитаем фиксированный дисконт) и PercentageDiscount (вычитаем процент), которые наследуют DiscountStrategy.
            Добавить конструктор с параметром: либо фиксированная сумма, либо процент.
            Переопределить метод ApplyDiscount и написать логику применения дисконта в соответствии с FixedDiscount либо PercentageDiscount.
            Создать класс Product с полями Name и Price.
            Создать класс ShoppingCart. Конструктор ShoppingCart принимает массив Product и также внутри инициализирует поле с типом DiscountStrategy.        
            Добавить метод SetDiscountStrategy, принимающий DiscountStrategy и меняющий внутри класса поле DiscountStrategy.
            Добавить метод CalculateTotal, который задействует DiscountStrategy.
            
            Создать несколько продуктов.
            Создать ShoppingCart, положив в конструктор массив Product.
            Создать два объекта: PercentageDiscount и FixedDiscount.
            Используя SetDiscountStrategy, назначьте конкретную стратегию дисконта 
            (важно отметить: если не использовать этот метод, то объект, который создался при инициализации ShoppingCart,
            будет использовать базовую реализацию, то есть никакой дисконт применяться не будет).
            Вызовите метод CalculateTotal, который пройдётся по каждому продукту,
            применит скидку и вернёт double-сумму уже со скидкой в зависимости от стратегии.
        */

        /*
            Дополнение к заданию 2 домашней работы номер №7

            Реализуйте стратегию оплаты, используя интерфейс IPaymentStrategy.
            Оплата может быть, к примеру, через ApplePay или картой — на ваше усмотрение.
            Соответственно, в IPaymentStrategy есть метод ProcessPayment(decimal amount).
            Создайте несколько реализаций IPaymentStrategy.

            Также создайте интерфейс ILogger с методом Log и сделайте одну реализацию логирования в консоль 
            (просто вывести в консоль с помощью Console.WriteLine) и вторую реализацию для логирования в файл (сообщение в консоли, что логирование производится в файл).

            Создайте класс PaymentProcessor, который будет принимать в конструктор IPaymentStrategy и ILogger.
            Добавьте метод ProcessPayment в классе PaymentProcessor, который будет логировать и выполнять метод ProcessPayment интерфейса IPaymentStrategy.

            Создайте экземпляр PaymentProcessor, а также несколько экземпляров различных видов оплат и разных логгеров.

            Код должен выглядеть примерно так:

            IPaymentStrategy creditCardPayment = new CreditCardPayment();
            ILogger consoleLogger = new ConsoleLogger();
            PaymentProcessor processor1 = new PaymentProcessor(creditCardPayment, consoleLogger);
            processor1.ProcessPayment(amount);

            // Using ApplePayPayment and FileLogger
            IPaymentStrategy applePayPayment = new ApplePayPayment();
            ILogger fileLogger = new FileLogger();
            PaymentProcessor processor2 = new PaymentProcessor(applePayPayment, fileLogger);
            processor2.ProcessPayment(amount);
        */

        public static void Example()
        {
            Product[] products1 = { new Product("Camera", 200), new Product("Printer", 500), new Product("Cooker", 1200) };
            Product[] products2 = { new Product("IPhone 11", 700), new Product("Samsung S23", 1000), new Product("Pixel 7 pro", 1500) };
            FixedDiscount fixedDiscount = new FixedDiscount(100);
            PercentageDiscount percentageDiscount = new PercentageDiscount(50);

            //Дополнение к домашнему заданию
            IPaymentStrategy creditCard = new CreditCard();
            ILogger consoleLogger = new ConsoleLogger();
            PaymentProcessor processor1 = new PaymentProcessor(creditCard, consoleLogger);

            IPaymentStrategy cash = new Cash();
            ILogger loggerToFile = new LoggerToFile();
            PaymentProcessor processor2 = new PaymentProcessor(cash, loggerToFile);

            ShoppingCart shoppingCart1 = new ShoppingCart(products1, processor1, fixedDiscount);
            shoppingCart1.CalculateTotal();     //200-100(fixDis)=100, 500-100(fixDis)=400, 1200-100(fixDis)=1100
            Console.WriteLine();

            ShoppingCart shoppingCart2 = new ShoppingCart(products2, processor2, percentageDiscount);
            shoppingCart2.CalculateTotal();     //700-350(procDis50%)=350, 1000-500(procDis50%)=500, 1500-750(procDis50%)=750
            Console.WriteLine();
        }
    }
}
