﻿using Lesson7.CarPark;
using Lesson7.Cars;
using Lesson7.Discount;
using Lesson7.AnimalNursery;
using System.Threading.Tasks;
using Lesson7;

internal class Program
{
    private static void Main(string[] args)
    {
        while (true)
        {
            Console.WriteLine(
            "\nWhat task will we choose(number)?\n" +
            "[1] Animal nursery\n" +
            "[2] Discount\n" +
            "[3] Car park\n" +
            "[4] Cars\n");

            if (!int.TryParse(Console.ReadLine(), out int numberTask))
            { Console.WriteLine("Incorrect value, please re-enter the operation"); continue; }
            Console.WriteLine();

            switch (numberTask)
            {
                case 1: ExampleAnimalNursery.Example(); break;
                case 2: ExampleDiscount.Example(); break;
                case 3: ExampleCarPark.Example(); break;
                case 4: ExampleCars.Example(); break;
                default: Console.WriteLine("Incorrect task number, please re-enter the number"); continue;
            }
        }
    } 
}